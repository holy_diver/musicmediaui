import { Routes } from "./definitions/httpclient";
import Cookies from 'js-cookie'
import axios, { AxiosPromise } from 'axios'
import CookieHandler from "../cookiehandler/cookiehandler";
import appConfig from '../configloader/configloader'

const cookieHandler = new CookieHandler();

const baseUrl = "http://" + appConfig.BackendDomain;

const routes: Routes = {
    Login: baseUrl + appConfig.LoginRoute,
    SocialTest: baseUrl + appConfig.SocialTestRoute
}

export default {
    get: function (url: string, config: any): Promise<AxiosPromise> {
        return axios.get(url, config);
    },
    post: function (url: string, data: any, config: any): Promise<AxiosPromise> {
        return axios.post(url, data, config);
    },
    auth: async (username: string, password: string, onError: (err: any) => void): Promise<boolean> => {
        let result = false;
        await axios.post(routes.Login, { username, password }).then((resp) => {
            if (resp.status == 200) {
                result = true;
                const authModel = resp.data as AuthModel;
                cookieHandler.SetAuthToken(authModel.JWTToken);
            }
        }).catch(onError);
        return result;
    }
}

